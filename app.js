var Express = require('express');
var app = Express();

app.get("/", function (req, res) {
    res.send("WELCOME!!!");
});

app.get("/about", function (req, res) {
    res.send("<h1> This is about page</h1>");
});
app.get("/thanks", function (req, res) {
    res.send("<h1> Thank You!!!!</h1>");
});

app.listen(3001, () => {
    console.log("Server is listening to port 3001....");
});